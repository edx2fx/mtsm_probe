#ifndef MCAST_CONTROLLER_HPP_INCLUDED
#define MCAST_CONTROLLER_HPP_INCLUDED

#include <algorithm>
#include <unordered_map>

#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

namespace mtsm_probe {

class McastController {
  const static int INACTIVE_SOCKET{-1};
  const static size_t SOCKET_BUFF_SIZE{1316*500};
  const static int PACKET_BUFF_SIZE{1316*10};

  // mpegtsmon/mpegts_udp.c
  const int MPEGTS_SIZE = 188;
  const int NULL_PID = 0x1FFF;
  const static uint8_t NO_COUNTER{0xFF};
  const static size_t PID_COUNT{0x1FFF+1};

  struct State {
    struct {
      uint32_t buffer_overflow_;
      uint32_t packet_count;
      uint32_t pids_num;
      uint32_t null_count;
      uint32_t scrambled;
      uint32_t tei;
      uint32_t sync_error;
      uint32_t error_count;
    };

    uint8_t counters[PID_COUNT];
    int len;
    State(): len{0} {
      std::fill_n(counters, sizeof(counters), static_cast<uint8_t>(NO_COUNTER));
      std::fill((uint8_t*)&buffer_overflow_, (uint8_t*)(&error_count+1), 0);
    }
  };

  struct Socket {
    int socket_;
    Socket() {
      socket_ = ::socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    }
    Socket(int s) {
      socket_ = s;
    }

    ~Socket() {
      if(socket_!=INACTIVE_SOCKET) close(socket_);
    };
  };

  struct SocketData {
    Socket socket_;
    State state_;
  };

  using SocketDataPtr = std::unique_ptr<SocketData>;
  using Mcast = std::pair<uint32_t, uint16_t>;
  struct HashMcast {
    size_t operator()(const Mcast &mcast ) const {
      return std::hash<uint32_t>()(mcast.first) ^ std::hash<uint16_t>()(mcast.second);
    }
  };

  std::unordered_map<int, SocketDataPtr> socket2data_;
  std::unordered_map<Mcast, int, HashMcast> mcast2socket_;

  State zero_state_;

  mutable bool is_modified_;

 public:
  McastController(): is_modified_{true} {
  }

  size_t count() const {
    return socket2data_.size();
  }

  inline bool is_modified() const {
    return is_modified_;
  }

  void check(uint32_t addr, uint16_t port) {
    modified(true);
    auto sd=SocketDataPtr{new SocketData()};
    int socket=sd->socket_.socket_;
    socket2data_.emplace(socket, std::move(sd));
    mcast2socket_.emplace(std::make_pair(addr, port), socket);

    // SO_REUSEADDR and SO_REUSEPORT http://www.unixguide.net/network/socketfaq/4.11.shtml
    int reuse = 1;
    setsockopt(socket, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse));

    int buff_size = SOCKET_BUFF_SIZE;
    setsockopt(socket, SOL_SOCKET, SO_RCVBUF, &buff_size, sizeof(buff_size));

    sockaddr_in mcast_addr{AF_INET, htons(port), htonl(addr)};
    bind(socket, (const sockaddr*)&mcast_addr, sizeof(mcast_addr));

    struct ip_mreq mreq {
      htonl(addr), htonl(INADDR_ANY)
    };
    setsockopt(socket, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof(mreq));
  }

  void stop(uint32_t addr, uint16_t port) {
    modified(true);
    auto si=mcast2socket_.find(std::make_pair(addr, port));
    if(si!=mcast2socket_.end()) {
      socket2data_.erase(si->second);
      mcast2socket_.erase(si);
    }
  }

  void stop() {
    modified(true);
    socket2data_.clear();
    mcast2socket_.clear();
  }

  void pollin(int socket) {
    uint8_t buff[PACKET_BUFF_SIZE+1];
    auto len=recvfrom(socket, buff, sizeof(buff), 0, NULL, NULL);

    auto& state=socket2data_[socket]->state_;

    if(len>PACKET_BUFF_SIZE) {
      ++state.buffer_overflow_;
      state.len=PACKET_BUFF_SIZE;
    } else {
      state.len=len;
    }
    check_errors(&state, buff);
  }

  const State& state(uint32_t addr, uint16_t port) const {
    auto si=mcast2socket_.find(std::make_pair(addr, port));
    if(si!=mcast2socket_.end()) {
      return socket2data_.find(si->second)->second->state_;
    }
    return zero_state_;
  }

  std::vector<int> sockets() const {
    std::vector<int> ss;
    for(auto& sd: socket2data_) ss.emplace_back(sd.first);
    modified(false);
    return ss;
  }

 private:
  // mpegtsmon/mpegts_udp.c
  void check_errors(State *d, uint8_t *buf, ssize_t from=0) {
    uint8_t *packet;
    for(packet = buf + from; packet < buf + d->len; packet += MPEGTS_SIZE) {
      if(packet[0] == 0x47) {
        ++d->packet_count;
        uint16_t pid = ((packet[1] & 0x1F) << 8) | packet[2];

        if(NULL_PID==pid) {
          ++d->null_count;
          continue;
        }

        d->tei += packet[1] >> 7;

        if(packet[3] >> 7) ++d->scrambled;

        uint8_t counter = packet[3] & 0x0F;
        uint8_t expected = d->counters[pid];

        if(NO_COUNTER == expected) ++d->pids_num;
        else if(!(expected == counter || expected - counter==1 || (0==expected && 15==counter))) { // CC may not change under certain conditions
          ++d->error_count;
        }
        d->counters[pid] = (counter + 1) & 0x0F;
      } else {
        d->sync_error = 1;
      }
    }
  }

  void modified(bool flag) const {
    is_modified_=flag;
  }
};

}
#endif // MCAST_CONTROLLER_HPP_INCLUDED

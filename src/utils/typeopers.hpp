#ifndef TYPEOPERS_HPP_INCLUDED
#define TYPEOPERS_HPP_INCLUDED

#include <cstddef>

namespace mtsm_probe {

// Type selector
// from Stroustrup's fourth edition

template<unsigned N, typename... Cases>
struct typeselect;

template<unsigned N, typename T, typename... Cases>
struct typeselect<N,T,Cases...>:typeselect<N-1,Cases...> {
};

template<typename T, typename ... Cases>
struct typeselect<0,T,Cases...> {
  using type = T;
};

template<unsigned N, typename... Cases>
using TypeSelect = typename typeselect<N,Cases...>::type;

// Calc offset
template<size_t idx,typename T, typename... Types> struct offset {
  static constexpr size_t calc() {
    static_assert(idx<sizeof...(Types)+1,"index over");
    return offset<idx-1, Types...>::calc()+sizeof(T);
  }
};

template<typename T, typename... Types> struct offset<0, T, Types...> {
  static constexpr size_t calc() {
    return 0;
  }
};

// Calc total size
template<size_t idx,typename T, typename... Types> struct total {
  static constexpr size_t calc() {
    static_assert(idx<sizeof...(Types)+1,"index over");
    return total<idx-1, Types...>::calc()+sizeof(T);
  }
};

template<typename T, typename... Types> struct total<0, T, Types...> {
  static constexpr size_t calc() {
    return sizeof(T);
  }
};

}
#endif // TYPEOPERS_HPP_INCLUDED
